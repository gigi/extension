import * as Messaging from './serviceWorker'

async function ping_test() {
    const res = await Messaging.message("background", "ping")
    console.log(res)
}

ping_test()
