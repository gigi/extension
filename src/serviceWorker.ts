import * as background from './ping'

const messages = {
    background
}
setupListener(messages)

type MyRoot = { [k: string]: { [l: string]: (...args: any[]) => any } }

interface TypedMessage<
    Root extends MyRoot,
    Type extends keyof Root,
    Command extends keyof Root[Type]
    > {
    type: Type
    command: Command
    args: Parameters<Root[Type][Command]>
}

function backgroundHandler<
    Root extends MyRoot,
    Type extends keyof Root,
    Command extends keyof Root[Type]
>(
    root: Root,
    message: TypedMessage<Root, Type, Command>,
): ReturnType<Root[Type][Command]> {
    return root[message.type][message.command](...message.args)
}

export function setupListener<Root extends MyRoot>(root: Root) {
    chrome.runtime.onMessage.addListener(
        (message: any) => {
            console.log(message)
            if (message.type in root) {
                if (!(message.command in root[message.type]))
                    throw new Error(
                        `missing handler in protocol ${message.type} ${message.command}`,
                    )
                if (!Array.isArray(message.args))
                    throw new Error(
                        `wrong arguments in protocol ${message.type} ${message.command}`,
                    )
                const val = Promise.resolve(backgroundHandler(root, message))
                console.log(val)
                return val
            }
        },
    )
}

export interface Message {
    [key: string]: any
    // and other unknown attributes...
}

export async function message<
    Type extends keyof MyRoot,
    Command extends keyof MyRoot[Type],
    F extends ((...args: any[]) => any) & MyRoot[Type][Command]
>(type: Type, command: Command, ...params: Parameters<F>) {
    const message: TypedMessage<MyRoot, Type, Command> = {
        type,
        command,
        args: params,
    }
    return chrome.runtime.sendMessage(message)
}


